--------------------------------------------------------------------------------
ret_timer = 0
local m = require "matrix"
--matrix initialization
--a matrix having 6 rows and 6 colomns having 0 at all
--another method rewards={{0,0,0,0,0,0},0,0,0,0,0,0},0,0,0,0,0,0},
--0,0,0,0,0,0},0,0,0,0,0,0},0,0,0,0,0,0}}


--for round of fucntion
function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end
--------------------------------to read the reward matrix-----------------------
function load_reward()
  local m = require "matrix"

  file = io.open("R.lua", "r")
  io.input(file)
  sr = io.read()
  inter = {}
  for i in string.gmatch(sr, "%S+") do
     table.insert(inter, tonumber(i))
  end
  file:close()
  r = m(6,6,0)
  for i = 1, 6 do
    r[1][i] = inter[i]
  end
  for i = 7, 11 do
    r[2][math.fmod(i,6)] = inter[i]
  end
  for i = 13, 17 do
    r[3][math.fmod(i,6)] = inter[i]
  end
  for i = 19, 23 do
    r[4][math.fmod(i,6)] = inter[i]
  end
  for i = 25, 29 do
    r[5][math.fmod(i,6)] = inter[i]
  end
  for i = 31, 35 do
    r[6][math.fmod(i,6)] = inter[i]
  end
  r[2][6] = inter[12]
  r[3][6] = inter[18]
  r[4][6] = inter[24]
  r[5][6] = inter[30]
  r[6][6] = inter[36]
  for i = 1,6 do
    for j = 1, 6 do
      log(r[i][j])
    end
  end
end
--[[
room no = row no & column number
room no ||| motor ground value
1 = 0.831373
2 = 0.658824
3 = 0
4 = 0.321569
5 = 0.74902
6 = 1
]]
--gate is a table
gate = {}
local m = require "matrix"


--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

function init()
  state = "roaming"
  prev_state = "dummy"
  load_reward()
  robot.colored_blob_omnidirectional_camera.enable()
end
--&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
function step()
  if state ~= prev_state then
    log(state)
  end
  prev_state = state
  if state == "roaming" then
    roaming()
  --[[elseif state == "gate_or_goal" then
    gate_or_goal()
  elseif state == "goal" then
    goal()]]--
  elseif state == "gate" then
    gate()
  elseif state == "gate_decision" then
    gate_decision()
  elseif state == "pass_gate" then
    pass_gate()
  elseif state == "same_room" then
    same_room()
  elseif state == "gate_to_gate" then
    gate_to_gate()
  elseif state == "goal_reached" then
    goal_reached()
  end
end
--------------------------------------------------------------------------------

function reset()
end



function destroy()
end
--------------------------------------------------------------------------------
function roaming()
  if (robot.motor_ground[1].value == 1 and
      robot.motor_ground[2].value == 1 and
      robot.motor_ground[3].value == 1 and
      robot.motor_ground[4].value == 1) then
        state= "goal_reached"
  end
  --log(robot.motor_ground[1].value) /for knowing the room no
  ret_timer = 0
  cr = robot.motor_ground[1].value
  cr = round(cr,2)
  if cr ==  0.83 then
    cr = 1
  elseif cr == 0.66 then
    cr = 2
  elseif cr == 0 then
    cr = 3
  elseif cr == 0.32 then
    cr = 4
  elseif cr ==0.75 then
    cr = 5
  elseif cr == 1 then
    cr = 6
  end
  --log(cr)
  sensingLeft =     robot.proximity[3].value +
                    robot.proximity[4].value +
                    robot.proximity[5].value +
                    robot.proximity[6].value +
                    robot.proximity[2].value

  sensingFront =    robot.proximity[1].value +
                    robot.proximity[24].value

  sensingRight =    robot.proximity[19].value +
                    robot.proximity[20].value +
                    robot.proximity[21].value +
                    robot.proximity[22].value +
                    robot.proximity[23].value
  if #robot.colored_blob_omnidirectional_camera > 0 then
    --robot.wheels.set_velocity(10,-10)
    state = "gate"
  elseif sensingLeft ~= 0  then
    robot.wheels.set_velocity(7,3)
  elseif sensingRight ~= 0 then
    robot.wheels.set_velocity(3,7)
  elseif sensingFront ~= 0 then
    a = robot.random.bernoulli(0.5)
    --log(a)
    if a == 0 then
      robot.wheels.set_velocity(90,1)
    elseif a == 1 then
      robot.wheels.set_velocity(1,90)
    end
  else
      robot.wheels.set_velocity(10,10)
  end
end

--------------------------------------------------------------------------------
----------------------What to do when a gate is found---------------------------
--------------------------------------------------------------------------------
--[[function gate_or_goal()
  if #robot.colored_blob_omnidirectional_camera > 0 then
    for i = 1, #robot.colored_blob_omnidirectional_camera do
      if robot.colored_blob_omnidirectional_camera[i].color.red == 255 then
        state = "goal"
      else
        state = "gate"
      end
    end
  end
end]]--
--------------------------------------------------------------------------------
--[[function goal()
  if #robot.colored_blob_omnidirectional_camera > 0 then
      x = 0
      for i = 1, 24 do --some modification must be done here as we need not check
                       --all proximity sensors then ones located in front shall do
          if x < robot.proximity[i].value then
              x = robot.proximity[i].value
          end
      end
-------trying to keep the orientation while approaching the obstacle------------
      dist = robot.colored_blob_omnidirectional_camera[1].distance
      ang =  robot.colored_blob_omnidirectional_camera[1].angle

      for i = 1, #robot.colored_blob_omnidirectional_camera do

          if dist > robot.colored_blob_omnidirectional_camera[i].distance and
              robot.colored_blob_omnidirectional_camera[i].color.red == 255 then

              dist = robot.colored_blob_omnidirectional_camera[i].distance
              ang = robot.colored_blob_omnidirectional_camera[i].angle

          end
      end
      if ang > 0 then
          robot.wheels.set_velocity(3,6)
      end
      if ang < 0 then
          robot.wheels.set_velocity(6,3)
      end
      if ang == 0 then
          robot.wheels.set_velocity(5,5)
      end
-------------trying to slow down when reaching near the obstacle----------------
      if x >= 0.5 then
          robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
      end
      if x >= 0.8 then
          robot.wheels.set_velocity(10,-10)
          log("Destination Reached")
      end
  end
end]]--
--------------------------------------------------------------------------------
--------------------------------function gate()---------------------------------
--------------------------------------------------------------------------------
function gate()
  if (robot.motor_ground[1].value == robot.motor_ground[2].value and
      robot.motor_ground[2].value == robot.motor_ground[3].value and
      robot.motor_ground[3].value == robot.motor_ground[4].value) then
    if #robot.colored_blob_omnidirectional_camera > 0 then
-------trying to keep the orientation while approaching the obstacle------------
        dist = robot.colored_blob_omnidirectional_camera[1].distance
        ang =  robot.colored_blob_omnidirectional_camera[1].angle

        for i = 1, #robot.colored_blob_omnidirectional_camera do

            if dist > robot.colored_blob_omnidirectional_camera[i].distance and
                robot.colored_blob_omnidirectional_camera[i].color.green == 255 then

                dist = robot.colored_blob_omnidirectional_camera[i].distance
                ang = robot.colored_blob_omnidirectional_camera[i].angle

            end
        end
        if ang > 0 then
            robot.wheels.set_velocity(3,6)
        end
        if ang < 0 then
            robot.wheels.set_velocity(6,3)
        end
        if ang == 0 then
          robot.wheels.set_velocity(5,5)
        end
    end
  else
    state = "gate_decision"
  end
end
--------------------------------------------------------------------------------
--[[function gate_decision()
  robot.wheels.set_velocity(0,0)
  x = robot.random.bernoulli(0.5)
  log(x)
  if x == 1 then
    state = "pass_gate"
  elseif x == 0 then
    state = "same_room"
  end
end]]--
function gate_decision()
  robot.wheels.set_velocity(0,0)
  a = round(robot.motor_ground[1].value,2)
  for i = 2, 4 do
    if robot.motor_ground[i]. value ~= robot.motor_ground[1].value then
      b = round(robot.motor_ground[i].value,2)
    end
  end
  if a >=  0.80 and a <= 0.87 then
    a = 1
  elseif a >= 0.62 and a <= 0.70 then
    a = 2
  elseif a >= 0 and a <= 0.05 then
    a = 3
  elseif a >= 0.28 and a <= 0.42 then
    a = 4
  elseif a >=0.71 and a <= 0.79 then
    a = 5
  elseif a >= 0.96 and a <= 1 then
    a = 6
  end
  if b >=  0.80 and b <= 0.87 then
    b = 1
  elseif b >= 0.62 and b <= 0.70 then
    b = 2
  elseif b >= 0 and b <= 0.05 then
    b = 3
  elseif b >= 0.28 and b <= 0.42 then
    b = 4
  elseif b >=0.71 and b <= 0.79 then
    b = 5
  elseif b >= 0.96 and b <= 1 then
    b = 6
  end
  log(a)
  log(b)
  if a == cr and b ~= cr then
    q_learning(a,b)
  elseif a ~= cr and b == cr then
    q_learning(b,a)
  end
end
--------------------------------------------------------------------------------
function pass_gate()
  if #robot.colored_blob_omnidirectional_camera == 1 then
    robot.wheels.set_velocity(7,7)
  elseif #robot.colored_blob_omnidirectional_camera == 0 then
    state = "roaming"
  elseif #robot.colored_blob_omnidirectional_camera == 2 then
    state = "gate_to_gate"
  end
end
--------------------------------------------------------------------------------
function same_room()
  if ret_timer < 26 then
    --log(ret_timer)
    robot.wheels.set_velocity(5,-5)
    ret_timer = ret_timer + 1
  elseif (#robot.colored_blob_perspective_camera == 0 and
      #robot.colored_blob_omnidirectional_camera ==1) then
    robot.wheels.set_velocity(10,10)
  elseif (#robot.colored_blob_perspective_camera == 0 and
      #robot.colored_blob_omnidirectional_camera == 0) then
    state = "roaming"
  end
end
--------------------------------------------------------------------------------
function gate_to_gate()
  if (robot.motor_ground[1].value == robot.motor_ground[2].value and
      robot.motor_ground[2].value == robot.motor_ground[3].value and
      robot.motor_ground[3].value == robot.motor_ground[4].value) then
    if #robot.colored_blob_omnidirectional_camera > 0 then
-------trying to keep the orientation while approaching the obstacle------------
        dist = robot.colored_blob_omnidirectional_camera[1].distance
        ang =  robot.colored_blob_omnidirectional_camera[1].angle

        for i = 1, #robot.colored_blob_omnidirectional_camera do

            if ((robot.colored_blob_omnidirectional_camera[i].angle < 1.5708) or
                (robot.colored_blob_omnidirectional_camera[i].angle > 4.71239 and
                  robot.colored_blob_omnidirectional_camera[i].angle < 6.4)) then

                dist = robot.colored_blob_omnidirectional_camera[i].distance
                ang = robot.colored_blob_omnidirectional_camera[i].angle

            end
        end
        if ang > 0 then
            robot.wheels.set_velocity(3,6)
        end
        if ang < 0 then
            robot.wheels.set_velocity(6,3)
        end
        if ang == 0 then
          robot.wheels.set_velocity(5,5)
        end
    end
  else
    state = "gate_decision"
  end
end
--------------------------------------------------------------------------------
function q_learning(cur, nxt)
  log("currentroom: ", cur)
  log("next room: ", nxt)
  --log(r[cur][nxt])
  ---------UPDATING REWARD MATRIX-
  gift = 0
  for i = 1,6 do
    if r[nxt][i] >= gift then
      gift = r[nxt][i]
    end
  end
  r[cur][nxt] = r[cur][nxt] + 0.8*gift
  if (nxt == 6 and r[cur][nxt] < 100) then
    r[cur][nxt] = 100
  end
----deciding wether to pass or not
  max_value = r[cur][nxt]
  for i = 1,6 do
    if r[cur][i] > max_value then
      max_value = r[cur][i]
    end
  end
  if max_value == r[cur][nxt] then
    state = "pass_gate"
  else
    state = "same_room"
  end

end
--------------------------------------------------------------------------------
function goal_reached()
  robot.wheels.set_velocity(10,-10)
  f = io.open("R.lua","w")
  io.output(f)
  for i = 1,6 do
    s= ''
    for j = 1, 6 do
      s = s..' '..tostring(r[i][j])
    end
    --print(s)
    f:write(s)
  end
  f:close()
  log("done")
end
