--------------------------------------------------------------------------------
gate = {}
local m = require "matrix"
function init()
  state = "roaming"
  prev_state = "dummy"
  robot.colored_blob_omnidirectional_camera.enable()
end

function step()
  if state ~= prev_state then
    log(state)
  end
  prev_state = state
  if state == "roaming" then
    roaming()
  elseif state == "gate" then
    gate()
  elseif state == "goal" then
    goal()
  elseif state == "gate_decision" then
    gate_decision()
  end
end
--------------------------------------------------------------------------------
function reset()
end
function destroy()
end
--------------------------------------------------------------------------------
function roaming()

  sensingLeft =     robot.proximity[3].value +
                    robot.proximity[4].value +
                    robot.proximity[5].value +
                    robot.proximity[6].value +
                    robot.proximity[2].value

  sensingFront =    robot.proximity[1].value +
                    robot.proximity[24].value

  sensingRight =    robot.proximity[19].value +
                    robot.proximity[20].value +
                    robot.proximity[21].value +
                    robot.proximity[22].value +
                    robot.proximity[23].value
  if #robot.colored_blob_omnidirectional_camera > 0 then
    --robot.wheels.set_velocity(10,-10)
    state = "gate"
  elseif sensingLeft ~= 0  then
    robot.wheels.set_velocity(7,5)
  elseif sensingRight ~= 0 then
    robot.wheels.set_velocity(5,7)
  elseif sensingFront ~= 0 then
    a = robot.random.bernoulli(0.5)
    --log(a)
    if a == 0 then
      robot.wheels.set_velocity(90,1)
    elseif a == 1 then
      robot.wheels.set_velocity(1,90)
    end
  else
      robot.wheels.set_velocity(10,10)
  end
end
--------------------------------------------------------------------------------
----------------------What to do when a gate is found---------------------------
--------------------------------------------------------------------------------
function gate()
  if #robot.colored_blob_omnidirectional_camera > 0 then
    for i = 1, #robot.colored_blob_omnidirectional_camera do
      if robot.colored_blob_omnidirectional_camera[i].color.red == 255 then
        state = "goal"
      else
        state = "gate_decision"
      end
    end
  end
end
--------------------------------------------------------------------------------
function goal()
  if #robot.colored_blob_omnidirectional_camera > 0 then
      x = 0
      for i = 1, 24 do --some modification must be done here as we need not check
                       --all proximity sensors then ones located in front shall do
          if x < robot.proximity[i].value then
              x = robot.proximity[i].value
          end
      end
-------trying to keep the orientation while approaching the obstacle------------
      dist = robot.colored_blob_omnidirectional_camera[1].distance
      ang =  robot.colored_blob_omnidirectional_camera[1].angle

      for i = 1, #robot.colored_blob_omnidirectional_camera do

          if dist > robot.colored_blob_omnidirectional_camera[i].distance and
              robot.colored_blob_omnidirectional_camera[i].color.red == 255 then

              dist = robot.colored_blob_omnidirectional_camera[i].distance
              ang = robot.colored_blob_omnidirectional_camera[i].angle

          end
      end
      if ang > 0 then
          robot.wheels.set_velocity(3,6)
      end
      if ang < 0 then
          robot.wheels.set_velocity(6,3)
      end
      if ang == 0 then
          robot.wheels.set_velocity(5,5)
      end
-------------trying to slow down when reaching near the obstacle----------------
      if x >= 0.5 then
          robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
      end
      if x >= 0.9 then
          robot.wheels.set_velocity(10,-10)
          log("Destination Reached")
      end
  end
end
--------------------------------------------------------------------------------
--------------------------gate decisions----------------------------------------
--------------------------------------------------------------------------------
function gate_position()
  r_ang = robot.positioning.orientation.angle
  r_s = robot.positioning.orientation.axis.z
  r_x = robot.positioning.position.x
  r_y = robot.positioning.position.y
  if r_s == -1 then
    r_ang = 6.28319 - robot.positioning.orientation.angle
  end
  d = light.distance/100
  log(d)
  l_ang = light.angle
  x1 = d * (math.cos(l_ang))
  y1 = d * (math.sin(l_ang))
  t = m{{math.cos(r_ang- 6.28319), -math.sin(r_ang - 6.28319)}, {math.sin(r_ang - 6.28319), math.cos(r_ang - 6.28319)}}
  rel = m{{x1}, {y1}}
  absolute_coordinates = t * rel
  log((absolute_coordinates[1][1] - r_x), " ", (absolute_coordinates[2][1] - r_y))
end
function gate_decision()
  dist = 10000
  if #robot.colored_blob_omnidirectional_camera > 0 then
    for i = 1, #robot.colored_blob_omnidirectional_camera do
      if robot.colored_blob_omnidirectional_camera[i].distance < dist and
        robot.colored_blob_omnidirectional_camera[i].color.green == 255 then

        light = robot.colored_blob_omnidirectional_camera[i]

      end
    end
  end

  gate_position()
end
